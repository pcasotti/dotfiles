#
# ~/.bashrc
#

export WINEDEBUG=-all
export __GL_THREADED_OPTIMIZATIONS=1
export _JAVA_AWT_WM_NONREPARENTING=1

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='exa -la --color=always --group-directories-first --icons'

alias please='sudo "$BASH" -c "$(history -p !!)"'
alias pls='please'

PS1='\[\033[31m\][\[\033[32m\]\u\[\033[34m\]@\[\033[34m\]\h \[\033[35m\]\W\[\033[31m\]]\[\033[93m\]☭\[\033[37m\] '

macchina -t Lithium -o processor-load -o memory

if [ -f $HOME/.cargo/env ] ; then
    . "$HOME/.cargo/env"
fi

if [ -f /usr/share/fzf/key-bindings.bash ] ; then
    source /usr/share/fzf/key-bindings.bash
fi
if [ -f /usr/share/fzf/completion.bash ] ; then
    source /usr/share/fzf/completion.bash
fi
if command -v fzf-share >/dev/null; then
  source "$(fzf-share)/key-bindings.bash"
  source "$(fzf-share)/completion.bash"
fi
export FZF_DEFAULT_OPTS=" \
    --color=bg+:#313244,bg:#1e1e2e,spinner:#f5e0dc,hl:#f38ba8 \
    --color=fg:#cdd6f4,header:#f38ba8,info:#cba6f7,pointer:#f5e0dc \
    --color=marker:#f5e0dc,fg+:#cdd6f4,prompt:#cba6f7,hl+:#f38ba8"
