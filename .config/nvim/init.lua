local path_package = vim.fn.stdpath('data') .. '/site'
local mini_path = path_package .. '/pack/deps/start/mini.nvim'
if not vim.loop.fs_stat(mini_path) then
  vim.cmd('echo "Installing `mini.nvim`" | redraw')
  local clone_cmd = {
    'git', 'clone', '--filter=blob:none',
    'https://github.com/echasnovski/mini.nvim', mini_path
  }
  vim.fn.system(clone_cmd)
  vim.cmd('packadd mini.nvim | helptags ALL')
end

require('mini.deps').setup({ path = { package = path_package } })

vim.g.mapleader = " "
vim.g.maplocalleader = " "

vim.keymap.set({ "i", "n" }, "<esc>", "<cmd>noh<cr><esc>", { desc = "Escape and clear hlsearch" })
vim.keymap.set("t", "<esc><esc>", "<c-\\><c-n>", { desc = "Exit terminal mode" })

vim.keymap.set("n", "<leader>w", "<cmd>w<cr>", { desc = "Save" })
vim.keymap.set("n", "<leader>qw", "<cmd>wq<cr>", { desc = "Save and Quit" })
vim.keymap.set("n", "<leader>qW", "<cmd>wqa<cr>", { desc = "Save and Quit All" })
vim.keymap.set("n", "<leader>qq", "<cmd>q<cr>", { desc = "Quit" })
vim.keymap.set("n", "<leader>qa", "<cmd>qa<cr>", { desc = "Quit All" })

-- vim.opt.clipboard = "unnamedplus" -- Sync with system clipboard
-- vim.opt.completeopt = "menu,menuone,noselect"
-- vim.opt.confirm = true -- Confirm to save changes before exiting modified buffer
-- vim.opt.cursorline = true -- Enable highlighting of the current line
-- vim.opt.expandtab = true -- Use spaces instead of tabs
-- vim.opt.formatoptions = "jcroqlnt" -- tcqj
-- vim.opt.grepformat = "%f:%l:%c:%m"
-- vim.opt.grepprg = "rg --vimgrep"
-- vim.opt.ignorecase = true -- Ignore case
-- vim.opt.inccommand = "nosplit" -- preview incremental substitute
-- vim.opt.laststatus = 0
-- vim.opt.list = true -- Show some invisible characters (tabs...
-- vim.opt.mouse = "a" -- Enable mouse mode
-- vim.opt.number = true -- Print line number
-- vim.opt.pumheight = 10 -- Maximum number of entries in a popup
-- vim.opt.relativenumber = true -- Relative line numbers
-- vim.opt.scrolloff = 4 -- Lines of context
-- vim.opt.shiftround = true -- Round indent
-- vim.opt.shiftwidth = 4 -- Size of an indent
-- vim.opt.shortmess:append({ W = true, I = true, c = true })
-- vim.opt.showmode = false -- Dont show mode since we have a statusline
-- vim.opt.sidescrolloff = 8 -- Columns of context
-- vim.opt.signcolumn = "yes" -- Always show the signcolumn, otherwise it would shift the text each time
-- vim.opt.smartcase = true -- Don't ignore case with capitals
-- vim.opt.smartindent = true -- Insert indents automatically
-- vim.opt.spelllang = { "en" }
-- vim.opt.splitbelow = true -- Put new windows below current
-- vim.opt.splitright = true -- Put new windows right of current
-- vim.opt.tabstop = 4 -- Number of spaces tabs count for
-- vim.opt.termguicolors = true -- True color support
-- vim.opt.undofile = true
-- vim.opt.undolevels = 10000
-- vim.opt.updatetime = 200 -- Save swap file and trigger CursorHold
-- vim.opt.wildmode = "longest:full,full" -- Command-line completion mode
-- vim.opt.winminwidth = 5 -- Minimum window width
-- vim.opt.wrap = false -- Disable line wrap
-- vim.opt.title = true
-- vim.opt.titleold = ""
-- vim.opt.colorcolumn = "100"

-- Show line numbers
vim.opt.number = true;
vim.opt.relativenumber = true;

-- Enable mouse mode
vim.opt.mouse = "a";

-- Don't show the mode, since it's already in the statusline
vim.opt.showmode = false;

-- Sync clipboard between OS and Neovim
vim.opt.clipboard = "unnamedplus";

-- Enable break indent
vim.opt.breakindent = true;

-- Save undo history
vim.opt.undofile = true;
vim.opt.undolevels = 10000;

-- Case-insensitive searching UNLESS \C or one or more capital letters in search term
vim.opt.ignorecase = true;
vim.opt.smartcase = true;

-- Keep signcolumn on by default
vim.opt.signcolumn = "yes";

-- Decrease update time
vim.opt.updatetime = 200;

-- Configure how new splits should be opened
vim.opt.splitright = true;
vim.opt.splitbelow = true;

-- Show whitespace characters
vim.opt.list = true;

-- Preview subsitutions live, as you type!
vim.opt.inccommand = "split";

-- Show which line your cursor is on
vim.opt.cursorline = true;

-- Minimal number of screen lines to keep above and below the cursor
vim.opt.scrolloff = 10;

-- Set highlight on search, but clear on pressing <Esc> in normal mode
vim.opt.hlsearch = true;

-- confirm to save changes before exiting modified buffer
vim.opt.confirm = true;

-- Use spaces instead of tabs
vim.opt.expandtab = true;

-- Size of an indent
vim.opt.shiftwidth = 4;

-- Number of spaces tabs count for
vim.opt.tabstop = 4;

-- tcqj
vim.opt.formatoptions = "jcrqlnt";

-- Command-line completion mode
vim.opt.wildmode = "longest:full,full";

-- Disable line wrap
vim.opt.wrap = false;

-- Change terminal title
vim.opt.title = true;
vim.opt.titleold = "";

-- Vertical column at 100
vim.opt.colorcolumn = "100";

-- Insert indents automatically
vim.opt.smartindent = true;

vim.opt.completeopt = "menu,menuone,noselect"

-- True color support
vim.opt.termguicolors = true

vim.opt.laststatus = 0

local add, now, later = MiniDeps.add, MiniDeps.now, MiniDeps.later

now(function()
  add({ source = 'catppuccin/nvim', name = 'catppuccin' })
  require("catppuccin").setup({
    transparent_background = true,

    integrations = {
      leap = true,
      beacon = true,
      noice = true,
      fidget = true,
    }
  })
  vim.cmd.colorscheme 'catppuccin'
end)

now(function()
  add({ source = 'j-hui/fidget.nvim' })
  require('fidget').setup({
    notification = {
      window = {
        winblend = 0,
      },
    }
  })
end)

now(function()
  add({ source = 'stevearc/dressing.nvim' })
  require('dressing').setup()
end)

now(function()
  add({
    source = 'folke/noice.nvim',
    depends = {
      'MunifTanjim/nui.nvim',
      'hrsh7th/nvim-cmp'
    }
  })
  require('noice').setup({
    lsp = {
      -- override markdown rendering so that **cmp** and other plugins use **Treesitter**
      override = {
        ['vim.lsp.util.convert_input_to_markdown_lines'] = true,
        ['vim.lsp.util.stylize_markdown'] = true,
        ['cmp.entry.get_documentation'] = true, -- requires hrsh7th/nvim-cmp
      },
    },
    -- you can enable a preset for easier configuration
    presets = {
      bottom_search = true, -- use a classic bottom cmdline for search
      command_palette = true, -- position the cmdline and popupmenu together
      long_message_to_split = false, -- long messages will be sent to a split
      inc_rename = false, -- enables an input dialog for inc-rename.nvim
      lsp_doc_border = true, -- add a border to hover docs and signature help
    },
  })
end)

now(function()
  add({ source = 'folke/persistence.nvim' })
  require('persistence').setup()
  vim.keymap.set('n', '<leader>qs', function() require('persistence').load() end, { desc = 'Load the session for the current directory' })
  vim.keymap.set('n', '<leader>qS', function() require('persistence').select() end, { desc = 'Select a session to load' })
  vim.keymap.set('n', '<leader>ql', function() require('persistence').load({ last = true }) end, { desc = 'Load the last session' })
  vim.keymap.set('n', '<leader>qd', function() require('persistence').stop() end, { desc = "Stop Persistence => session won't be saved on exit" })
end)

now(function()
  add({ source = 'nvim-tree/nvim-web-devicons' })
  require('nvim-web-devicons').setup()
  require('mini.icons').setup()
  MiniIcons.mock_nvim_web_devicons()
end)

now(function()
  add({ source = 'lewis6991/gitsigns.nvim' })
  require('gitsigns').setup()
  local gitsigns = require('gitsigns')

  local function map(mode, l, r, d)
    opts = {
      buffer = bufnr,
      desc = d
    }
    vim.keymap.set(mode, l, r, opts)
  end

  -- Navigation
  map('n', ']h', function()
    if vim.wo.diff then
      vim.cmd.normal({']h', bang = true})
    else
      gitsigns.nav_hunk('next')
    end
  end)

  map('n', '[h', function()
    if vim.wo.diff then
      vim.cmd.normal({'[h', bang = true})
    else
      gitsigns.nav_hunk('prev')
    end
  end)

  -- Actions
  map('n', '<leader>hs', gitsigns.stage_hunk, 'Stage hunk')
  map('n', '<leader>hr', gitsigns.reset_hunk, 'Reset hunk')
  map('v', '<leader>hs', function() gitsigns.stage_hunk {vim.fn.line('.'), vim.fn.line('v')} end, 'Stage hunk')
  map('v', '<leader>hr', function() gitsigns.reset_hunk {vim.fn.line('.'), vim.fn.line('v')} end, 'Reset hunk')
  map('n', '<leader>hS', gitsigns.stage_buffer, 'Stage buffer')
  map('n', '<leader>hu', gitsigns.undo_stage_hunk, 'Ustage hunk')
  map('n', '<leader>hR', gitsigns.reset_buffer, 'Reset buffer')
  map('n', '<leader>hp', gitsigns.preview_hunk, 'Preview hunk')
  map('n', '<leader>hb', function() gitsigns.blame_line{full=true} end, 'Blame line')
  map('n', '<leader>tb', gitsigns.blame, 'Open blame tab')
  map('n', '<leader>hd', gitsigns.diffthis, 'Diffthis')
  map('n', '<leader>hD', function() gitsigns.diffthis('~') end, 'Diffthis')
  map('n', '<leader>td', gitsigns.toggle_deleted, 'Toggle deleted')

  -- Text object
  map({'o', 'x'}, 'ih', ':<C-U>Gitsigns select_hunk<CR>', 'Select hunk')
end)

now(function()
  add({
    source = 'nvim-treesitter/nvim-treesitter',
    depends = { 'LiadOz/nvim-dap-repl-highlights' },
    hooks = { post_checkout = function() vim.cmd('TSUpdate') end },
  })
  require('nvim-dap-repl-highlights').setup()
  require('nvim-treesitter.configs').setup({
    highlight = {
      enable = true,
      additional_vim_regex_highlighting = false,
    },
    ensure_installed = { 'dap_repl' },
    auto_install = true,
  })
end)

now(function()
  add({ source = 'luukvbaal/statuscol.nvim' })
  local builtin = require("statuscol.builtin")
  require("statuscol").setup({
    segments = {
      { text = { "%s" }, click = "v:lua.ScSa" },
      {
        text = { builtin.lnumfunc, " " },
        condition = { true, builtin.not_empty },
        click = "v:lua.ScLa",
      },
      -- { text = { "%C" }, click = "v:lua.ScFa" }
      { text = { builtin.foldfunc, " " }, click = "v:lua.ScFa" }
    },
  })
end)

now(function()
  add({
    source = 'kevinhwang91/nvim-ufo',
    depends = { 'kevinhwang91/promise-async' }
  })
  vim.opt.foldcolumn = '1' -- '0' is not bad
  vim.opt.foldlevel = 99 -- Using ufo provider need a large value, feel free to decrease the value
  vim.opt.foldlevelstart = 99
  vim.opt.foldenable = true
  vim.opt.fillchars = [[eob: ,fold: ,foldopen:,foldsep: ,foldclose:]]
  -- vim.opt.foldcolumn = 'auto:9'

  -- Using ufo provider need remap `zR` and `zM`. If Neovim is 0.6.1, remap yourself
  vim.keymap.set('n', 'zR', function()
    require('ufo').openAllFolds()
    vim.opt.foldlevel = 99
  end)
  vim.keymap.set('n', 'zM', require('ufo').closeAllFolds)

  -- Option 3: treesitter as a main provider instead
  -- (Note: the `nvim-treesitter` plugin is *not* needed.)
  -- ufo uses the same query files for folding (queries/<lang>/folds.scm)
  -- performance and stability are better than `foldmethod=nvim_treesitter#foldexpr()`
  require('ufo').setup({
    provider_selector = function(bufnr, filetype, buftype)
      return {'treesitter', 'indent'}
    end
  })
  --
end)

now(function() require('mini.statusline').setup() end)

later(function()
  require('mini.basics').setup({
    options = {
      basic = false,
      extra_ui = false,
      win_borders = 'default',
    },
    mappings = {
      basic = true,
      option_toggle_prefix = [[\]],
      windows = true,
      move_with_alt = false,
    },
    autocommands = {
      basic = true,
      relnum_in_visual_mode = false,
    },
    silent = false,
  })
end)

later(function() require('mini.extra').setup() end)

later(function() require('mini.splitjoin').setup() end)

later(function()
  add({ source = 'nmac427/guess-indent.nvim' })
  require('guess-indent').setup()
end)

later(function()
  require('mini.misc').setup()
  vim.keymap.set('n', '<leader>ff', function() MiniMisc.zoom() end, { desc = 'Zoom window' })
end)

later(function()
  require('mini.indentscope').setup({
    symbol = '│',
    options = { try_as_border = true }
  })
end)

later(function()
  require('mini.bufremove').setup()
  vim.keymap.set('n', 'X', function() MiniBufremove.delete() end, { desc = 'Close buffer' })
end)

later(function() require('mini.bracketed').setup() end)

later(function()
  local miniclue = require('mini.clue')
  miniclue.setup({
    triggers = {
      -- Leader triggers
      { mode = 'n', keys = '<Leader>' },
      { mode = 'x', keys = '<Leader>' },

      -- Built-in completion
      { mode = 'i', keys = '<C-x>' },

      -- `g` key
      { mode = 'n', keys = 'g' },
      { mode = 'x', keys = 'g' },

      -- Marks
      { mode = 'n', keys = "'" },
      { mode = 'n', keys = '`' },
      { mode = 'x', keys = "'" },
      { mode = 'x', keys = '`' },

      -- Registers
      { mode = 'n', keys = '"' },
      { mode = 'x', keys = '"' },
      { mode = 'i', keys = '<C-r>' },
      { mode = 'c', keys = '<C-r>' },

      -- Window commands
      { mode = 'n', keys = '<C-w>' },

      -- `z` key
      { mode = 'n', keys = 'z' },
      { mode = 'x', keys = 'z' },

      -- Brackets
      { mode = 'n', keys = '[' },
      { mode = 'n', keys = ']' },

      -- Basics
      { mode = 'n', keys = [[\]] },
    },

    clues = {
      -- Enhance this by adding descriptions for <Leader> mapping groups
      miniclue.gen_clues.builtin_completion(),
      miniclue.gen_clues.g(),
      miniclue.gen_clues.marks(),
      miniclue.gen_clues.registers(),
      miniclue.gen_clues.windows(),
      miniclue.gen_clues.z(),
    },
  })
end)

later(function()
  require('mini.files').setup({ windows = { preview = true, width_preview = 50 } })
  vim.keymap.set('n', '<leader>nt', function() MiniFiles.open(vim.api.nvim_buf_get_name(0)) end, { desc = 'Open directory of current file' })
  vim.keymap.set('n', '<leader>nT', function() MiniFiles.open(nil, false) end, { desc = 'Open current working directory' })
end)

later(function()
  require('mini.pick').setup()

  local orig_picker = MiniExtra.pickers.lsp
  MiniExtra.pickers.lsp = function(local_opts, opts)
    if local_opts.scope == 'definition' then
      vim.lsp.buf.definition({ on_list = function(data)
        if #data.items == 1 then
          vim.fn.setqflist({}, ' ', data)
          vim.cmd.cfirst()
        else
          orig_picker(local_opts, opts)
        end
      end})
    else
      orig_picker(local_opts, opts)
    end
  end

  local wipeout_cur = function()
    vim.api.nvim_buf_delete(MiniPick.get_picker_matches().current.bufnr, {})
  end
  local buffer_mappings = { wipeout = { char = '<C-d>', func = wipeout_cur } }

  local ns_id = vim.api.nvim_create_namespace('MiniPickPathHead')
  local show = function(buf_id, items_arr, query)
    local lines = vim.tbl_map(function(x)
      path = x.text or x
      return {
        text = vim.fn.fnamemodify(path, ':t')..'  '..vim.fn.fnamemodify(path, ':.:h')..'/' ,
        path = path
      }
    end, items_arr)
    MiniPick.default_show(buf_id, lines, query, { show_icons = true })

    vim.api.nvim_buf_clear_namespace(buf_id, ns_id, 0, -1)
    local buf_lines = vim.api.nvim_buf_get_lines(buf_id, 0, -1, false)
    for i, line in ipairs(buf_lines) do
      local _, match, _ = string.find(line, '  ')
      match = match or 0
      vim.api.nvim_buf_set_extmark(buf_id, ns_id, i-1, match, {
        end_row = i-1,
        end_col = string.len(line),
        hl_group = 'Comment',
        hl_mode = 'combine',
        priority = 100
      })
    end
  end

  vim.keymap.set('n', '<leader><tab>', function() MiniPick.builtin.buffers(nil, { mappings = buffer_mappings, source = { show = show } }) end, { desc = 'Pick buffers' })
  vim.keymap.set('n', '<leader>o', function() MiniPick.builtin.files(nil, { source = { show = show } }) end, { desc = 'Pick files' })
  vim.keymap.set('n', '<leader>fg', function() MiniPick.builtin.grep_live() end, { desc = 'Pick grep' })
  vim.keymap.set('n', '<leader>fr', function() MiniPick.builtin.grep({ pattern = vim.fn.expand('<cword>') }) end, { desc = 'Pick grep word' })
  vim.keymap.set('n', '<leader>fp', function() MiniPick.builtin.resume() end, { desc = 'Pick resume' })
  vim.keymap.set('n', '<leader>fh', function() MiniPick.builtin.help() end, { desc = 'Pick help' })
  vim.keymap.set('n', '<leader>fl', function() MiniExtra.pickers.diagnostic() end, { desc = 'Pick all diagnostics' })
  vim.keymap.set('n', '<leader>ft', function() MiniExtra.pickers.diagnostic({ scope = 'current' }) end, { desc = 'Pick local diagnostics' })
  vim.keymap.set('n', '<leader>?', function() MiniExtra.pickers.keymaps() end, { desc = 'Pick keymaps' })
  vim.keymap.set('n', '<leader>d', function() MiniExtra.pickers.lsp({ scope = 'definition' }) end, { desc = 'Pick lsp definition' })
  vim.keymap.set('n', '<leader>i', function() MiniExtra.pickers.lsp({ scope = 'implementation' }) end, { desc = 'Pick lsp implementation' })
  vim.keymap.set('n', '<leader>r', function() MiniExtra.pickers.lsp({ scope = 'references' }) end, { desc = 'Pick lsp references' })
end)

later(function() require('mini.operators').setup() end)

later(function() require('mini.surround').setup({ search_method = 'cover_or_next' }) end)

later(function()
  require('mini.pairs').setup({
    mappings = {
      ['('] = { action = 'open', pair = '()', neigh_pattern = '[^\\][^%a]' },
      ['['] = { action = 'open', pair = '[]', neigh_pattern = '[^\\][^%a]' },
      ['{'] = { action = 'open', pair = '{}', neigh_pattern = '[^\\][^%a]' },
      ['<'] = { action = 'open', pair = '<>', neigh_pattern = '[%a^\\].' },

      [')'] = { action = 'close', pair = '()', neigh_pattern = '[^\\].' },
      [']'] = { action = 'close', pair = '[]', neigh_pattern = '[^\\].' },
      ['}'] = { action = 'close', pair = '{}', neigh_pattern = '[^\\].' },
      ['>'] = { action = 'close', pair = '<>', neigh_pattern = '[^\\].' },

      ['"'] = { action = 'closeopen', pair = '""', neigh_pattern = '[^\\].', register = { cr = false } },
      ["'"] = { action = 'closeopen', pair = "''", neigh_pattern = '[^%a\\].', register = { cr = false } },
      ['`'] = { action = 'closeopen', pair = '``', neigh_pattern = '[^\\].', register = { cr = false } },
    }
  })
end)

later(function() require('mini.jump').setup({ delay = { highlight = 10000000 } }) end)

later(function()
  require('mini.ai').setup({
    custom_textobjects = {
      L = MiniExtra.gen_ai_spec.line()
    }
  })
end)

later(function()
  add({
    source = 'hrsh7th/nvim-cmp',
    depends = {
      'hrsh7th/cmp-buffer',
      'hrsh7th/cmp-path',
      'hrsh7th/cmp-cmdline',
      'L3MON4D3/LuaSnip',
      'saadparwaiz1/cmp_luasnip',
      'petertriho/cmp-git',
      'nvim-lua/plenary.nvim',
      'onsails/lspkind.nvim',
      'hrsh7th/cmp-nvim-lsp',
      'rcarriga/cmp-dap',
      'mfussenegger/nvim-dap',
    }
  })

  local cmp = require('cmp')
  cmp.setup({
    enabled = function()
      local disabled = false
      disabled = disabled or (vim.api.nvim_buf_get_option(0, 'buftype') == 'prompt'
        and not require('cmp_dap').is_dap_buffer())
      disabled = disabled or (vim.fn.reg_recording() ~= '')
      disabled = disabled or (vim.fn.reg_executing() ~= '')
      return not disabled
    end,

    snippet = {
      expand = function(args)
        require('luasnip').lsp_expand(args.body) -- For `luasnip` users.
        -- vim.snippet.expand(args.body) -- For native neovim snippets (Neovim v0.10+)
      end,
    },
    -- window = {
    --   completion = cmp.config.window.bordered(),
    --   documentation = cmp.config.window.bordered(),
    -- },
    window = {
      completion = {
        winhighlight = "Normal:Pmenu,FloatBorder:Pmenu,Search:None",
        col_offset = -3,
        side_padding = 0,
      },
      completion = cmp.config.window.bordered(),
      documentation = cmp.config.window.bordered(),
    },
    formatting = {
      fields = { "kind", "abbr", "menu" },
      format = function(entry, vim_item)
        local kind = require("lspkind").cmp_format({ mode = "symbol_text", maxwidth = 50 })(entry, vim_item)
        local strings = vim.split(kind.kind, "%s", { trimempty = true })
        kind.kind = " " .. (strings[1] or "") .. " "
        kind.menu = "    (" .. (strings[2] or "") .. ")"

        return kind
      end,
    },
    -- view = {
    --   entries = 'native'
    -- },
    -- formatting = {
    --   format = require('lspkind').cmp_format()
    -- },
    mapping = cmp.mapping.preset.insert({
      -- ["<C-n>"] = cmp.mapping.select_next_item({ behavior = cmp.SelectBehavior.Insert }),
      -- ["<C-p>"] = cmp.mapping.select_prev_item({ behavior = cmp.SelectBehavior.Insert }),
      ['<C-b>'] = cmp.mapping.scroll_docs(-4),
      ['<C-f>'] = cmp.mapping.scroll_docs(4),
      ['<C-Space>'] = cmp.mapping.complete(),
      ['<C-e>'] = cmp.mapping.abort(),
      ['<CR>'] = cmp.mapping.confirm({ select = true }), -- Accept currently selected item. Set `select` to `false` to only confirm explicitly selected items.
    }),
    sources = cmp.config.sources({
      { name = 'nvim_lsp' },
      { name = 'luasnip' }, -- For luasnip users.
    }, {
      { name = 'buffer' },
    })
  })

  -- To use git you need to install the plugin petertriho/cmp-git and uncomment lines below
  -- Set configuration for specific filetype.
  cmp.setup.filetype('gitcommit', {
    sources = cmp.config.sources({
      { name = 'git' },
    }, {
      { name = 'buffer' },
    })
  })
  require("cmp_git").setup()

  require('cmp').setup.filetype({ 'dap-repl', 'dapui_watches', 'dapui_hover' }, {
    sources = {
      { name = 'dap' },
    },
  })

  -- Use buffer source for `/` and `?` (if you enabled `native_menu`, this won't work anymore).
  cmp.setup.cmdline({ '/', '?' }, {
    mapping = cmp.mapping.preset.cmdline(),
    sources = {
      { name = 'buffer' }
    }
  })

  -- Use cmdline & path source for ':' (if you enabled `native_menu`, this won't work anymore).
  cmp.setup.cmdline(':', {
    mapping = cmp.mapping.preset.cmdline(),
    sources = cmp.config.sources({
      { name = 'path' }
    }, {
      { name = 'cmdline' }
    }),
    matching = { disallow_symbol_nonprefix_matching = false }
  })
end)

later(function()
  add({ source = 'neovim/nvim-lspconfig' })
  local capabilities = require('lsp').capabilities
  local on_attach = require('lsp').on_attach

  local lspconfig = require('lspconfig')
  lspconfig.clangd.setup({
    on_attach = on_attach,
    capabilities = capabilities,
  })

  lspconfig.pylsp.setup({
    on_attach = on_attach,
    capabilities = capabilities,
  })

  local orig_util_open_floating_preview = vim.lsp.util.open_floating_preview
  function vim.lsp.util.open_floating_preview(contents, syntax, opts, ...)
    opts = opts or {}
    opts.border = opts.border or 'rounded'
    return orig_util_open_floating_preview(contents, syntax, opts, ...)
  end
end)

later(function()
  add({ source = 'mrcjkb/rustaceanvim' })
  vim.g.rustaceanvim = {
    -- -- Plugin configuration
    -- tools = {
    -- },
    -- -- LSP configuration
    server = {
      on_attach = function(client, bufnr)
        require('lsp').on_attach(client, bufnr)
        -- vim.keymap.set('n', '<leader>l', function() vim.cmd.RustLsp({ 'renderDiagnostic', 'current' }) end, {buffer = bufnr, remap = false})
        vim.keymap.set('n', 'J', function() vim.cmd.RustLsp('joinLines') end, {buffer = bufnr, remap = false})
      end,
      default_settings = {
        -- rust-analyzer language server configuration
        ['rust-analyzer'] = {
          cargo = {
            allTargets = true,
            features = 'all',
          },
        },
      },
    },
    -- DAP configuration
    dap = {
      adapter = require('rustaceanvim.config').get_codelldb_adapter(os.getenv('CODELLDB'), os.getenv('LIBLLDB')),
    },
  }
end)

later(function()
  add({ source = 'saecki/crates.nvim' })
  require('crates').setup({
    lsp = {
      enabled = true,
      on_attach = require('lsp').on_attach,
      actions = true,
      completion = true,
      hover = true,
    },
    completion = {
      crates = {
        enabled = true, -- disabled by default
        max_results = 8, -- The maximum number of search results to display
        min_chars = 3, -- The minimum number of charaters to type before completions begin appearing
      }
    }
  })
end)

later(function()
  add({
    source = 'mfussenegger/nvim-dap',
    depends = {
      'nvim-lua/plenary.nvim',
      'nvim-telescope/telescope.nvim'
    }
  })

  local dap = require('dap')

  local pickers = require("telescope.pickers")
  local finders = require("telescope.finders")
  local conf = require("telescope.config").values
  local actions = require("telescope.actions")
  local action_state = require("telescope.actions.state")

  dap.configurations.java = {
    {
        type = 'java',
        request = 'launch',
        name = "Test Class",
        foo = function() require('jdtls').test_class() end,
    },
    {
        type = 'java',
        request = 'launch',
        name = "Nearest Method",
        foo = function() require('jdtls').test_nearest_method() end,
    },
    {
      type = 'java';
      request = 'attach';
      name = "Debug (Attach)";
      hostName = "127.0.0.1";
      port = 5005;
      foo = function()
        return coroutine.create(function(coro)
          local opts = {}
          pickers
          .new(opts, {
            prompt_title = "Pocess to attach",
            finder = finders.new_oneshot_job({ "lsof", "-Pani", "-T", "-c", ".java" }, {}),
            sorter = conf.generic_sorter(opts),
            attach_mappings = function(buffer_number)
              actions.select_default:replace(function()
                actions.close(buffer_number)
                local l = action_state.get_selected_entry()[1]:gmatch("%S+")
                l();l();l();l();l();l();l();l();
                local a = l():gmatch('([^%:]+)')
                require('dap').configurations.java[1].hostName = a();
                require('dap').configurations.java[1].port = a();
                coroutine.resume(coro, action_state.get_selected_entry()[1])
              end)
              return true
            end,
          })
          :find()
        end)
      end,
    },
    {
      type = 'java';
      request = 'attach';
      name = "Attach - 127.0.0.1:5005";
      hostName = "127.0.0.1";
      port = 5005;
    },
  }

  vim.fn.sign_define("DapBreakpoint", { text = "●", texthl = "DapBreakpoint", linehl = "", numhl = ""})
  vim.fn.sign_define("DapBreakpointCondition", { text = "●", texthl = "DapBreakpointCondition", linehl = "", numhl = ""})
  vim.fn.sign_define("DapLogPoint", { text = "◆", texthl = "DapLogPoint", linehl = "", numhl = ""})

  vim.keymap.set('n', "<leader>uc", function() dap.continue() end, { desc = "Continue" })
  vim.keymap.set('n', "<leader>uC", function() dap.run_to_cursor() end, { desc = "Run to Cursor" })
  vim.keymap.set('n', "<leader>ug", function() dap.goto_() end, { desc = "Go to line (no execute)" })
  vim.keymap.set('n', "<leader>ui", function() dap.step_into({askForTargets=true}) end, { desc = "Step Into" })
  vim.keymap.set('n', "<leader>uj", function() dap.down() end, { desc = "Down" })
  vim.keymap.set('n', "<leader>uk", function() dap.up() end, { desc = "Up" })
  vim.keymap.set('n', "<leader>ul", function() dap.run_last() end, { desc = "Run Last" })
  vim.keymap.set('n', "<leader>uO", function() dap.step_out() end, { desc = "Step Out" })
  vim.keymap.set('n', "<leader>uo", function() dap.step_over() end, { desc = "Step Over" })
  vim.keymap.set('n', "<leader>up", function() dap.pause() end, { desc = "Pause" })
  vim.keymap.set('n', "<leader>ur", function() dap.repl.toggle() end, { desc = "Toggle REPL" })
  vim.keymap.set('n', "<leader>us", function() dap.session() end, { desc = "Session" })
  vim.keymap.set('n', "<leader>ut", function() dap.terminate() end, { desc = "Terminate" })
  vim.keymap.set({'n', 'v'}, "<leader>uw", function() require("dap.ui.widgets").hover() end, { desc = "Widgets" })
end)

later(function()
  add({
    source = 'mfussenegger/nvim-dap-python',
    depends = {
      'mfussenegger/nvim-dap',
    }
  })
  require("dap-python").setup("python")
end)

later(function()
  add({
    source = 'rcarriga/nvim-dap-ui',
    depends = {
      'mfussenegger/nvim-dap',
      'nvim-neotest/nvim-nio'
    }
  })
  require('dapui').setup()
  vim.keymap.set('n', "<leader>uu", function() require('dapui').toggle() end, { desc = "Dap UI" })
  vim.keymap.set({'n', 'v'}, "<leader>ue", function() require('dapui').eval() end, { desc = "Eval" })
end)

later(function()
  add({ source = 'Weissle/persistent-breakpoints.nvim' })
  require('persistent-breakpoints').setup({ load_breakpoints_event = { "BufReadPost" } })
  vim.keymap.set('n', "<leader>ub", function() require("persistent-breakpoints.api").toggle_breakpoint() end, { desc = "Toggle Breakpoint" })
  vim.keymap.set('n', "<leader>uB", function() require("persistent-breakpoints.api").set_conditional_breakpoint(vim.fn.input('[Condition] > ')) end, { desc = "Breakpoint Condition" })
  vim.keymap.set('n', "<leader>ugb", function() require("persistent-breakpoints.api").clear_breakpoints() end, { desc = "Clear Breakpoints" })
end)

later(function() add({ source = 'mfussenegger/nvim-jdtls' }) end)

later(function()
  add({ source = 'lukas-reineke/indent-blankline.nvim' })
  require('ibl').setup({
    indent = {
      char = "│"
    },
    scope = {
      enabled = false
    },
  })
end)

later(function()
  add({ source = 'alexghergh/nvim-tmux-navigation' })
  local nvim_tmux_nav = require('nvim-tmux-navigation')
  nvim_tmux_nav.setup({}) -- This breaks without {}

  vim.keymap.set('n', "<C-h>", nvim_tmux_nav.NvimTmuxNavigateLeft)
  vim.keymap.set('n', "<C-j>", nvim_tmux_nav.NvimTmuxNavigateDown)
  vim.keymap.set('n', "<C-k>", nvim_tmux_nav.NvimTmuxNavigateUp)
  vim.keymap.set('n', "<C-l>", nvim_tmux_nav.NvimTmuxNavigateRight)
end)

later(function()
  add({
    source = 'ggandor/leap.nvim',
    depends = { 'tpope/vim-repeat' }
  })
  require('leap').opts.safe_labels = {}
  require('leap').opts.preview_filter = function () return false end
  vim.api.nvim_set_hl(0, 'LeapBackdrop', { link = 'Comment' })
  vim.keymap.set({'n', 'x', 'o'}, '<enter>',  '<Plug>(leap)')
end)

later(function()
  add({ source = 'DanilaMihailov/beacon.nvim' })
  require('beacon').setup()
end)
