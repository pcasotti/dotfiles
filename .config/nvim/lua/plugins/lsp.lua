local C = require('paths')

local capabilities = require('lsp').capabilities
local on_attach = require('lsp').on_attach

return {
  {
    "folke/neodev.nvim",
    opts = {},
  },
  {
    "p00f/clangd_extensions.nvim",
    opts = {},
  },
  {
    'mfussenegger/nvim-jdtls',
    dependencies = {
      'neovim/nvim-lspconfig',
      'mfussenegger/nvim-dap',
    },
  },
  {
    'simrat39/rust-tools.nvim',
    dependencies = {
      'neovim/nvim-lspconfig',
      'nvim-lua/plenary.nvim',
      'mfussenegger/nvim-dap',
    },
    config = function()
      require("rust-tools").setup({
        server = {
          on_attach = function(_, bufnr)
            on_attach(_, bufnr)

            local rt = require("rust-tools")
            vim.keymap.set("n", "<leader>a", rt.code_action_group.code_action_group, { buffer = bufnr })
            vim.keymap.set("n", "<s-k>", rt.hover_actions.hover_actions, { buffer = bufnr })
          end,
        },
        dap = {
          adapter = require('rust-tools.dap').get_codelldb_adapter(
            C.codelldb.codelldb, C.codelldb.liblldb
          )
        },
      })
    end,
  },
  {
    "neovim/nvim-lspconfig",
    dependencies = {
      { "hrsh7th/cmp-nvim-lsp" },
      {
        "SmiteshP/nvim-navic",
	    opts = { highlight = true },
      },
      {
        "SmiteshP/nvim-navbuddy",
        dependencies = {
          "SmiteshP/nvim-navic",
          "MunifTanjim/nui.nvim"
        },
        opts = { lsp = { auto_attach = true } },
        keys = {
          { "<leader>nn", function() require("nvim-navbuddy").open() end, mode = { "n" }, desc = "Open Navbuddy" },
        },
      },
    },
    config = function()
      -- TODO: Refactor this
      local lspconfig = require('lspconfig')
      lspconfig.glslls.setup({
        on_attach = on_attach,
        capabilities = capabilities,
      })
      local html_capabilities = vim.lsp.protocol.make_client_capabilities()
      html_capabilities.textDocument.completion.completionItem.snippetSupport = true
      lspconfig.html.setup({
        on_attach = on_attach,
        capabilities = html_capabilities,
      })
      lspconfig.eslint.setup({
        on_attach = on_attach,
        capabilities = capabilities,
      })
      lspconfig.clangd.setup({
        on_attach = on_attach,
        capabilities = capabilities,
      })
      lspconfig.pyright.setup({
        on_attach = on_attach,
        capabilities = capabilities,
      })
      lspconfig.vhdl_ls.setup({
        on_attach = on_attach,
        capabilities = capabilities,
      })
      lspconfig.hls.setup({
        on_attach = on_attach,
        capabilities = capabilities,
      })
      lspconfig.nil_ls.setup({
        on_attach = on_attach,
        capabilities = capabilities,
        settings = {
          ['nil'] = {
            formatting = {
              command = { "nixpkgs-fmt" },
            },
          },
        },
      })
      lspconfig.lua_ls.setup {
        on_attach = on_attach,
        capabilities = capabilities,
        settings = {
          Lua = {
            runtime = {
              -- Tell the language server which version of Lua you're using (most likely LuaJIT in the case of Neovim)
              version = 'LuaJIT',
            },
            diagnostics = {
              -- Get the language server to recognize the `vim` global
              globals = {'vim'},
            },
            workspace = {
              -- Make the server aware of Neovim runtime files
              library = vim.api.nvim_get_runtime_file("", true),
            },
            -- Do not send telemetry data containing a randomized but unique identifier
            telemetry = {
              enable = false,
            },
          },
        },
      }
    end,
  },
}
