return {
  {
    "catppuccin/nvim",
    lazy = false,
    priority = 1000,
    config = function()
      vim.cmd.colorscheme "catppuccin"
    end,
    name = "catppuccin",
    opts = {
      flavour = "mocha",
      integrations = {
        aerial = true,
        alpha = true,
        barbar = true,
        barbecue = {
            dim_dirname = true, -- directory name is dimmed by default
            bold_basename = true,
            dim_context = true,
            alt_background = true,
        },
        beacon = true,
        coc_nvim = true,
        dashboard = true,
        dropbar = {
            enabled = true,
            color_mode = true, -- enable color for kind's texts, not just kind's icons
        },
        fern = true,
        fidget = true,
        flash = true,
        gitsigns = true,
        harpoon = true,
        headlines = true,
        hop = true,
        indent_blankline = {
            enabled = true,
            colored_indent_levels = false,
        },
        leap = true,
        lightspeed = true,
        lsp_saga = true,
        markdown = true,
        mason = true,
        mini = true,
        neotree = true,
        neogit = true,
        neotest = true,
        noice = true,
        NormalNvim = true,
        notifier = true,
        cmp = true,
        dap = {
            enabled = true,
            enable_ui = true, -- enable nvim-dap-ui
        },
        native_lsp = {
            enabled = true,
            virtual_text = {
                errors = { "italic" },
                hints = { "italic" },
                warnings = { "italic" },
                information = { "italic" },
            },
            underlines = {
                errors = { "underline" },
                hints = { "underline" },
                warnings = { "underline" },
                information = { "underline" },
            },
            inlay_hints = {
                background = true,
            },
        },
        navic = {
            enabled = true,
            custom_bg = "NONE", -- "lualine" will set background to mantle
        },
        notify = true,
        semantic_tokens = true,
        nvimtree = true,
        treesitter_context = true,
        treesitter = true,
        ts_rainbow2 = true,
        ts_rainbow = true,
        ufo = true,
        window_picker = true,
        octo = true,
        overseer = true,
        pounce = true,
        rainbow_delimiters = true,
        symbols_outline = true,
        telekasten = true,
        telescope = {
            enabled = true,
        },
        lsp_trouble = true,
        gitgutter = true,
        illuminate = {
            enabled = true,
            lsp = true,
        },
        sandwich = true,
        vim_sneak = true,
        vimwiki = true,
        which_key = true,
      },
    },
  },
}
