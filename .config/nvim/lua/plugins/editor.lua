return {
  {
    "nvim-neo-tree/neo-tree.nvim",
    cmd = { "Neotree" },
    dependencies = {
      "nvim-lua/plenary.nvim",
      "nvim-tree/nvim-web-devicons",
      "MunifTanjim/nui.nvim",
    },
    keys = {
      -- { "<leader>nt", "<cmd>Neotree float toggle<cr>", mode = { "n" } },
      { "<leader>ng", "<cmd>Neotree float git_status<cr>", mode = { "n" } },
    },
  },
  {
    "echasnovski/mini.files",
    opts = {
      windows = {
        preview = true,
        width_preview = 50,
      },
    },
    keys = {
      {
        "<leader>nT",
        function()
          require("mini.files").open(vim.api.nvim_buf_get_name(0), true)
        end,
        desc = "Open mini.files (directory of current file)",
      },
      {
        "<leader>nt",
        function()
          require("mini.files").open(vim.loop.cwd(), true)
        end,
        desc = "Open mini.files (cwd)",
      },
    },
  },
  {
    "folke/flash.nvim",
    event = "VeryLazy",
    opts = {
      modes = {
        char = {
          jump_labels = true
        },
      },
    },
    keys = {
      { "ss", mode = { "n", "o", "x" }, function() require("flash").jump() end, desc = "Flash" },
      { "S", mode = { "n", "o", "x" }, function() require("flash").treesitter() end, desc = "Flash Treesitter" },
      {
        "sl",
        function()
          require("flash").jump({
            action = function(match, state)
              vim.api.nvim_win_call(match.win, function()
                vim.api.nvim_win_set_cursor(match.win, match.pos)
                vim.diagnostic.open_float()
              end)
              state:restore()
            end,
          })
        end,
        mode = { "n", "o", "x" },
      },
    },
  },
  {
    "nvim-telescope/telescope.nvim",
    cmd = { "Telescope" },
    dependencies = { "nvim-lua/plenary.nvim" },
    keys = {
      -- { "<leader>o", function() require('telescope.builtin').find_files() end, mode = { "n" } },
      -- { "<leader><tab>", function() require('telescope.builtin').buffers() end, mode = { "n" } },
      -- { "<leader>fg", function() require('telescope.builtin').live_grep() end, mode = { "n" } },
      { "<leader>fr", function() require('telescope.builtin').grep_string() end, mode = { "n", "v" } },
      { "<leader>fc", function() require('telescope.builtin').commands() end, mode = { "n" } },
      { "<leader>\'", function() require('telescope.builtin').marks() end, mode = { "n" } },
      { "<leader>\"", function() require('telescope.builtin').registers() end, mode = { "n", "v" } },
      { "<leader>fj", function() require('telescope.builtin').jumplist() end, mode = { "n" } },
      { "<leader>fl", function() require('telescope.builtin').diagnostics() end, mode = { "n" } },
      { "<leader>d", function() require('telescope.builtin').lsp_definitions() end, mode = { "n" } },
    },
  },
  {
    "echasnovski/mini.pick",
    dependencies = { "nvim-tree/nvim-web-devicons" },
    config = function()
      local win_config = function()
        local height = math.floor(0.618 * vim.o.lines)
        local width = math.floor(0.618 * vim.o.columns)
        return {
          anchor = 'NW', height = height, width = width,
          row = math.floor(0.5 * (vim.o.lines - height)),
          col = math.floor(0.5 * (vim.o.columns - width)),
        }
      end
      require("mini.pick").setup({
        window = {
          config = win_config,
        },
      })
    end,
    keys = {
      { "<leader><tab>", function() require('mini.pick').builtin.buffers() end, mode = { "n" } },
      { "<leader>fg", function() require('mini.pick').builtin.grep_live() end, mode = { "n" } },
      { "<leader>o", function() require('mini.pick').builtin.files() end, mode = { "n" } },
    },
  },
  {
    "folke/which-key.nvim",
    event = "VeryLazy",
    init = function()
      vim.o.timeout = true
      vim.o.timeoutlen = 300
    end,
    opts = {
      window = {
        border = "single",
      },
    },
  },
  {
    "lewis6991/gitsigns.nvim",
    opts = {
      current_line_blame_opts = {
        virt_text_pos = 'right_align',
        delay = 0,
      },
      on_attach = function(bufnr)
        local gs = package.loaded.gitsigns

        local function map(mode, l, r, opts)
          opts = opts or {}
          opts.buffer = bufnr
          vim.keymap.set(mode, l, r, opts)
        end

        -- Navigation
        map('n', ']c', function()
          if vim.wo.diff then return ']c' end
          vim.schedule(function() gs.next_hunk() end)
          return '<Ignore>'
        end, {expr=true})

        map('n', '[c', function()
          if vim.wo.diff then return '[c' end
          vim.schedule(function() gs.prev_hunk() end)
          return '<Ignore>'
        end, {expr=true})

        -- Actions
        map('n', '<leader>hs', gs.stage_hunk)
        map('n', '<leader>hr', gs.reset_hunk)
        map('v', '<leader>hs', function() gs.stage_hunk {vim.fn.line('.'), vim.fn.line('v')} end)
        map('v', '<leader>hr', function() gs.reset_hunk {vim.fn.line('.'), vim.fn.line('v')} end)
        map('n', '<leader>hS', gs.stage_buffer)
        map('n', '<leader>hu', gs.undo_stage_hunk)
        map('n', '<leader>hR', gs.reset_buffer)
        map('n', '<leader>hp', gs.preview_hunk)
        map('n', '<leader>hb', function() gs.blame_line{full=true} end)
        map('n', '<leader>tb', gs.toggle_current_line_blame)
        map('n', '<leader>hd', gs.diffthis)
        map('n', '<leader>hD', function() gs.diffthis('~') end)
        map('n', '<leader>td', gs.toggle_deleted)

        -- Text object
        map({'o', 'x'}, 'ih', ':<C-U>Gitsigns select_hunk<CR>')
      end,
    },
  },
  {
    "RRethy/vim-illuminate",
    lazy = false,
    config = function() require("illuminate").configure() end,
    keys = {
      { "]]", function() require('illuminate').goto_next_reference() end, mode = { "n" } },
      { "[[", function() require('illuminate').goto_prev_reference() end, mode = { "n" } },
    },
  },
  {
    "echasnovski/mini.bufremove",
    keys = {
      { "<s-x>", function() require("mini.bufremove").delete(0, false) end, desc = "Delete Buffer" },
    },
  },
  {
    "folke/trouble.nvim",
    cmd = { "TroubleToggle", "Trouble" },
    opts = { use_diagnostic_signs = true },
    dependencies = { "nvim-tree/nvim-web-devicons" },
    keys = {
      { "<leader>hh", function() require("trouble").open("document_diagnostics") end, desc = "Document Diagnostics (Trouble)" },
      { "<leader>hH", function() require("trouble").open("workspace_diagnostics") end, desc = "Workspace Diagnostics (Trouble)" },
      { "<leader>xl", function() require("trouble").open("loclist") end, desc = "Location List (Trouble)" },
      { "<leader>xq", function() require("trouble").open("quickfix") end, desc = "Quickfix List (Trouble)" },
      { "<leader>r", function() require("trouble").open("lsp_references") end, desc = "LSP References (Trouble)" },
      { "<leader>xt", function() require("trouble").open("lsp_type_definitions") end, desc = "LSP Type Definitions (Trouble)" },
    },
  },
  {
    "folke/todo-comments.nvim",
    cmd = { "TodoTrouble", "TodoTelescope" },
    dependencies = { "nvim-lua/plenary.nvim" },
    keys = {
      { "<leader>ft", "<cmd>TodoTelescope<cr>", desc = "Todo" },
    },
  },
  {
    "christoomey/vim-tmux-navigator",
    keys = {
      { "<C-h>", "<cmd>TmuxNavigateLeft<cr>", mode = { "n" }, desc = "Go to left window" },
      { "<C-j>", "<cmd>TmuxNavigateDown<cr>", mode = { "n" }, desc = "Go to lower window" },
      { "<C-k>", "<cmd>TmuxNavigateUp<cr>", mode = { "n" }, desc = "Go to upper window" },
      { "<C-l>", "<cmd>TmuxNavigateRight<cr>", mode = { "n" }, desc = "Go to right window" },
    },
  },
  {
    "lukas-reineke/virt-column.nvim",
    opts = {},
  },
  {
    "xiyaowong/transparent.nvim",
    opts = {
      extra_groups = {
        "NormalFloat", -- plugins which have float panel such as Lazy, Mason, LspInfo
        "NvimTreeNormal" -- NvimTree
      },
    },
  },
  {
    "Aasim-A/scrollEOF.nvim",
    opts = {},
  },
  {
    "toppair/reach.nvim",
    opts = {
      show_current = true,
    },
    keys = {
      { "<leader>ff", function() require('reach').buffers() end, mode = { "n" } },
    },
  },
  {
    "Iron-E/nvim-libmodal",
    lazy = true,
    keys = {
      {
        "<leader>g",
        function()
          local modeInstruction = {
            j = 'Gitsigns next_hunk',
            k = 'Gitsigns prev_hunk',
          }
          require('libmodal').mode.enter('GIT', modeInstruction)
        end,
        mode = { "n" } },
    },
  },
  {
    "jbyuki/nabla.nvim",
    config = false,
    keys = {
      { "<leader>m", function() require('nabla').popup() end, mode = { "n" } },
    },
  },
  -- {
  --   "pcasotti/launch.nvim",
  --   dependencies = {
  --     "mfussenegger/nvim-dap",
  --     "rcarriga/nvim-notify",
  --   },
  --   opts = {},
  -- },
  {
    "nmac427/guess-indent.nvim",
    opts = {},
  }
}
