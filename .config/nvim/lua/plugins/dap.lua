local C = require('paths')

return {
  {
    "theHamsta/nvim-dap-virtual-text",
    opts = {},
  },
  {
    "mfussenegger/nvim-dap",
    dependencies = {
      {
        "rcarriga/nvim-dap-ui",
        keys = {
          { "<leader>uu", function() require("dapui").toggle({ }) end, desc = "Dap UI" },
          { "<leader>ue", function() require("dapui").eval() end, desc = "Eval", mode = {"n", "v"} },
        },
        opts = {},
        dependencies = {
          { "nvim-neotest/nvim-nio" },
        },
      },
      { "nvim-telescope/telescope.nvim" },
    },
    config = function()
      require("dap")
      local sign = vim.fn.sign_define
      sign("DapBreakpoint", { text = "●", texthl = "DapBreakpoint", linehl = "", numhl = ""})
      sign("DapBreakpointCondition", { text = "●", texthl = "DapBreakpointCondition", linehl = "", numhl = ""})
      sign("DapLogPoint", { text = "◆", texthl = "DapLogPoint", linehl = "", numhl = ""})

      local pickers = require("telescope.pickers")
      local finders = require("telescope.finders")
      local conf = require("telescope.config").values
      local actions = require("telescope.actions")
      local action_state = require("telescope.actions.state")

      require('dap').configurations.java = {
        {
          type = 'java';
          request = 'attach';
          name = "Debug (Attach)";
          hostName = "127.0.0.1";
          port = 5005;
          foo = function()
            return coroutine.create(function(coro)
              local opts = {}
              pickers
              .new(opts, {
                prompt_title = "Pocess to attach",
                finder = finders.new_oneshot_job({ "lsof", "-Pan", "-i" }, {}),
                sorter = conf.generic_sorter(opts),
                attach_mappings = function(buffer_number)
                  actions.select_default:replace(function()
                    actions.close(buffer_number)
                    local l = action_state.get_selected_entry()[1]:gmatch("%S+")
                    l();l();l();l();l();l();l();l();
                    local a = l():gmatch('([^%:]+)')
                    require('dap').configurations.java[1].hostName = a();
                    require('dap').configurations.java[1].port = a();
                    coroutine.resume(coro, action_state.get_selected_entry()[1])
                  end)
                  return true
                end,
              })
              :find()
            end)
          end,
        },
      }
      require('dap').adapters.cppdbg = {
        id = 'cppdbg',
        type = 'executable',
        command = C.cpptools_dap,
      }
      require('dap').configurations.cpp = {
        {
          name = "Launch an executable",
          type = "cppdbg",
          request = "launch",
          cwd = "${workspaceFolder}",
          program = function()
            return coroutine.create(function(coro)
              local opts = {}
              pickers
              .new(opts, {
                prompt_title = "Path to executable",
                finder = finders.new_oneshot_job({ "fd", "--hidden", "--no-ignore", "--type", "x" }, {}),
                sorter = conf.generic_sorter(opts),
                attach_mappings = function(buffer_number)
                  actions.select_default:replace(function()
                    actions.close(buffer_number)
                    coroutine.resume(coro, action_state.get_selected_entry()[1])
                  end)
                  return true
                end,
              })
              :find()
            end)
          end,
        },
      }
      require('dap').configurations.c = require('dap').configurations.cpp
    end,
    keys = {
      { "<leader>uc", function() require("dap").continue() end, desc = "Continue" },
      { "<leader>uC", function() require("dap").run_to_cursor() end, desc = "Run to Cursor" },
      { "<leader>ug", function() require("dap").goto_() end, desc = "Go to line (no execute)" },
      { "<leader>ui", function() require("dap").step_into({askForTargets=true}) end, desc = "Step Into" },
      { "<leader>uj", function() require("dap").down() end, desc = "Down" },
      { "<leader>uk", function() require("dap").up() end, desc = "Up" },
      { "<leader>ul", function() require("dap").run_last() end, desc = "Run Last" },
      { "<leader>uO", function() require("dap").step_out() end, desc = "Step Out" },
      { "<leader>uo", function() require("dap").step_over() end, desc = "Step Over" },
      { "<leader>up", function() require("dap").pause() end, desc = "Pause" },
      { "<leader>ur", function() require("dap").repl.toggle() end, desc = "Toggle REPL" },
      { "<leader>us", function() require("dap").session() end, desc = "Session" },
      { "<leader>ut", function() require("dap").terminate() end, desc = "Terminate" },
      { "<leader>uw", function() require("dap.ui.widgets").hover() end, desc = "Widgets", mode = {"n", "v"} },
    },
  },
  {
    "Weissle/persistent-breakpoints.nvim",
    event = "BufReadPost",
    opts = {
      load_breakpoints_event = { "BufReadPost" },
    },
    keys = {
      { "<leader>ub", function() require("persistent-breakpoints.api").toggle_breakpoint() end, desc = "Toggle Breakpoint" },
      { "<leader>uB", function() require("persistent-breakpoints.api").set_conditional_breakpoint() end, desc = "Breakpoint Condition" },
    },
  },
}
