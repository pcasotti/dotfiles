MiniDeps.add({ source = 'hrsh7th/cmp-nvim-lsp' })

return {
  capabilities = require('cmp_nvim_lsp').default_capabilities(),
  on_attach = function(client, bufnr)
    vim.keymap.set({ 'n', 'v' }, '<leader>a', function() vim.lsp.buf.code_action() end, {buffer = bufnr, remap = false})
    vim.keymap.set({ 'n', 'v' }, '<s-k>', function() vim.lsp.buf.hover() end, {buffer = bufnr, remap = false})
    vim.keymap.set({ 'n', 'i' }, '<c-s>', function() vim.lsp.buf.signature_help() end, {buffer = bufnr, remap = false})
    vim.keymap.set('n', '<leader>l', function() vim.diagnostic.open_float() end, {buffer = bufnr, remap = false})
    vim.keymap.set('n', '<leader>D', function() vim.lsp.buf.declaration() end, {buffer = bufnr, remap = false})
    vim.keymap.set('n', '<space>R', function() vim.lsp.buf.rename() end, {buffer = bufnr, remap = false})
  end,
}
-- return {
--   capabilities = require('cmp_nvim_lsp').default_capabilities(),
--   on_attach = function(client, bufnr)
--     if client.server_capabilities.documentSymbolProvider then
--       require('nvim-navic').attach(client, bufnr)
--     end
--     require("nvim-navbuddy").attach(client, bufnr)
--     vim.keymap.set({ 'n', 'v' }, '<leader>a', function() vim.lsp.buf.code_action() end, {buffer = bufnr, remap = false})
--     vim.keymap.set({ 'n', 'v' }, '<s-k>', function() vim.lsp.buf.hover() end, {buffer = bufnr, remap = false})
--     vim.keymap.set({ 'n', 'i' }, '<c-s>', function() vim.lsp.buf.signature_help() end, {buffer = bufnr, remap = false})
--     vim.keymap.set('n', '<leader>l', function() vim.diagnostic.open_float() end, {buffer = bufnr, remap = false})
--     vim.keymap.set('n', '<leader>D', function() vim.lsp.buf.declaration() end, {buffer = bufnr, remap = false})
--     vim.keymap.set('n', '<space>R', function() vim.lsp.buf.rename() end, {buffer = bufnr, remap = false})
--   end,
-- }
