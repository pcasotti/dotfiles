MiniDeps.add({ source = 'mfussenegger/nvim-jdtls' })

local config = {
  cmd = {
    os.getenv('JDTLS_PATH'),
    '-data', os.getenv('HOME')..'/.cache/jdtls/workspace/'..vim.fn.fnamemodify(vim.fn.getcwd(), ':p:h:t')
  },

  -- 💀
  -- This is the default if not provided, you can remove it. Or adjust as needed.
  -- One dedicated LSP server & client will be started per unique root_dir
  --
  -- vim.fs.root requires Neovim 0.10.
  -- If you're using an earlier version, use: require('jdtls.setup').find_root({'.git', 'mvnw', 'gradlew'}),
  -- root_dir = vim.fs.root(0, {".git", "mvnw", "gradlew"})
  root_dir = os.getenv('JDTLS_ROOT') or vim.fs.root(0, {".git", "mvnw", "gradlew", "pom.xml"}),

  -- Here you can configure eclipse.jdt.ls specific settings
  -- See https://github.com/eclipse/eclipse.jdt.ls/wiki/Running-the-JAVA-LS-server-from-the-command-line#initialize-request
  -- for a list of options
  settings = {
    java = {
      configuration = {
        maven = {
          userSettings = os.getenv('PWD')..'/settings.xml';
        }
      }
    }
  },
}

local bundles = {
  vim.fn.glob(os.getenv('JDTLS_DEBUG'), 1)
};
vim.list_extend(bundles, vim.split(vim.fn.glob(os.getenv('JDTLS_TEST'), 1), "\n"))

config['init_options'] = {
  bundles = bundles;
}

config['on_attach'] = require('lsp').on_attach

require('jdtls').start_or_attach(config)
