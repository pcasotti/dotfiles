#
# ~/.bash_profile
#

[[ -f ~/.bashrc ]] && . ~/.bashrc

if [[ -z $DISPLAY ]] && [[ $(tty) = /dev/tty1 ]]; then exec startx; fi
if [[ -z $DISPLAY ]] && [[ $(tty) = /dev/tty2 ]]; then exec sway; fi
if [ -f $HOME/.cargo/env ] ; then
    source $HOME/.cargo/env
fi
if [ -f $HOME/.nix-profile/etc/profile.d/hm-session-vars.sh ] ; then
    source $HOME/.nix-profile/etc/profile.d/hm-session-vars.sh
fi
if [ -f /etc/profiles/per-user/$USER/etc/profile.d/hm-session-vars.sh ] ; then
    source /etc/profiles/per-user/$USER/etc/profile.d/hm-session-vars.sh
fi
